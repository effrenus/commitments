use "./hw3_types.sml";

val string_formatter = fn s => s
val int_formatter = Int.toString
val bool_formatter = Bool.toString

(* Ad hoc formatters *)
fun valu_formatter v =
    case v of
          Const v' => "Const(" ^ (int_formatter v') ^ ")"
        | Unit  => "Unit"
        | Tuple ls => "Tuple(" ^ (String.concatWith "," (List.map valu_formatter ls)) ^ ")"
        | Constructor(name, v') => "Costructor:" ^ name ^ "(" ^ (valu_formatter v') ^ ")"


fun a_list_formatter element_formatter list =
    case list of
	[] => "[]"
      | elements => "[" ^ (String.concatWith ", " (List.map element_formatter elements)) ^ "]"

fun a_option_formatter value_formatter option =
    case option of
	NONE => "NONE"
      | SOME value => "SOME " ^ value_formatter(value)

val string_list_formatter = a_list_formatter string_formatter 
val int_list_formatter = a_list_formatter int_formatter
val bool_list_formatter = a_list_formatter bool_formatter
val tuple3_int_list_formatter = a_list_formatter (fn (i1, i2, i3) => "(" ^ int_formatter i1 ^ "," ^ int_formatter i2 ^ "," ^ int_formatter i3 ^ ")")

val string_list_option_formatter = a_option_formatter string_list_formatter
val int_list_option_formatter = a_option_formatter int_list_formatter
val bool_list_option_formatter = a_option_formatter bool_list_formatter

val string_option_formatter = a_option_formatter string_formatter
val int_option_formatter = a_option_formatter int_formatter
val bool_option_formatter = a_option_formatter bool_formatter
val tuple3_int_option_formatter = a_option_formatter (fn (i1, i2, i3) => "(" ^ int_formatter i1 ^ "," ^ int_formatter i2 ^ "," ^ int_formatter i3 ^ ")")

val valu_list_formatter = a_list_formatter (fn (name : string, v : valu) => name ^ "::" ^ (valu_formatter v))
fun valu_list_option_formatter (v : (string * valu) list option) = a_option_formatter valu_list_formatter v