use "./test-lib/testing.sml";
use "./hw3.sml";

open SmlTests;

(* only_capitals *)
test("'only_capitals'",
     assert_equals_string_list (only_capitals ["Hello", "world", "World"], ["Hello", "World"]));

test("'only_capitals' - empty list",
     assert_equals_string_list (only_capitals [], []));

test("'only_capitals' - all lowercase",
     assert_equals_string_list (only_capitals ["who", "am", "i"], []));

test("'only_capitals' - one char string",
     assert_equals_string_list (only_capitals ["w", "Q", "i"], ["Q"]));

(* longest_string1 *)
test("'longest_string1'",
     assert_equals_string (longest_string1 ["abc", "abcde", "a"], "abcde"));

test("'longest_string1' - empty list",
     assert_equals_string (longest_string1 [], ""));

test("'longest_string1' - only one item",
     assert_equals_string (longest_string1 ["a"], "a"));

test("'longest_string1' - take first",
     assert_equals_string (longest_string1 ["ab", "cd", "ef"], "ab"));

(* longest_string2 *)
test("'longest_string2'",
     assert_equals_string (longest_string2 ["abc", "abcde", "a"], "abcde"));

test("'longest_string2' - empty list",
     assert_equals_string (longest_string2 [], ""));

test("'longest_string2' - only one item",
     assert_equals_string (longest_string2 ["a"], "a"));

test("'longest_string2' - take last",
     assert_equals_string (longest_string2 ["ab", "cd", "ef"], "ef"));

(* longest_string3 *)
test("'longest_string3'",
     assert_equals_string (longest_string3 ["abc", "abcde", "a"], "abcde"));

test("'longest_string3' - empty list",
     assert_equals_string (longest_string3 [], ""));

test("'longest_string3' - only one item",
     assert_equals_string (longest_string3 ["a"], "a"));

test("'longest_string3' - take first",
     assert_equals_string (longest_string3 ["ab", "cd", "ef"], "ab"));

(* longest_string4 *)
test("'longest_string4'",
     assert_equals_string (longest_string4 ["abc", "abcde", "a"], "abcde"));

test("'longest_string4' - empty list",
     assert_equals_string (longest_string4 [], ""));

test("'longest_string4' - only one item",
     assert_equals_string (longest_string4 ["a"], "a"));

test("'longest_string4' - take last",
     assert_equals_string (longest_string4 ["ab", "cd", "ef"], "ef"));

(* rev_string *)
test("'rev_string' - empty",
     assert_equals_string (rev_string "", ""));

test("'rev_string'",
     assert_equals_string (rev_string "abcd", "dcba"));

test("'rev_string'",
     assert_equals_string (rev_string "12345", "54321"));


(* first_answer *)
test("'first_answer'",
     assert_equals_int (first_answer (fn v => if v = 42 then SOME 43 else NONE) [40, 41, 42], 43));

test("'first_answer'",
     assert_equals_int (first_answer (fn v => if v = 40 then SOME 44 else NONE) [40], 44));

test("'first_answer' - empty",
     assert_raises_curry2 (first_answer, (fn _ => NONE), [], NoAnswer));

test("'first_answer' - return NONE for all elements",
     assert_raises_curry2 (first_answer, (fn _ => NONE), [40, 41, 42], NoAnswer));

(* all_answers *)
test("'all_answers' - return SOME for all element",
     assert_equals_int_list_option (all_answers (fn v => SOME [v + 1]) [40, 41, 42], SOME [43, 42, 41]));

test("'all_answers' - polymorphic function",
     assert_equals_string_list_option (all_answers (fn v => SOME [v ^ " world", v ^ "!"]) ["hello"], SOME ["hello world", "hello!"]));

test("'all_answers' - empty list",
     assert_equals_int_list_option (all_answers (fn v => NONE) [], SOME []));

test("'all_answers' - return NONE for last element",
     assert_equals_int_list_option (all_answers (fn v => if v = 42 then NONE else SOME [v + 1]) [40, 41, 42], NONE));

(* count_wildcards – exact 1 *)
test("'count_wildcards'",
     assert_equals_int (count_wildcards Wildcard, 1));

test("'count_wildcards' - several in TupleP",
     assert_equals_int (count_wildcards (TupleP [Wildcard, Variable "var", Wildcard, Wildcard]), 3));

test("'count_wildcards' - TupleP w/o Wildcard",
     assert_equals_int (count_wildcards (TupleP [Variable "var"]), 0));

test("'count_wildcards' - ConstructorP with Wildcard",
     assert_equals_int (count_wildcards (ConstructorP ("cons", TupleP [ConstP 42,Variable "var", Wildcard, UnitP])), 1));

test("'count_wildcards' - Nested ConstructorP with Wildcard",
     assert_equals_int (count_wildcards (ConstructorP ("cons", TupleP [ConstP 42, ConstructorP ("cons2", Wildcard), Variable "var", Wildcard, UnitP])), 2));

(* count_wild_and_variable_lengths *)
test("'count_wild_and_variable_lengths'",
     assert_equals_int (count_wild_and_variable_lengths Wildcard, 1));

test("'count_wild_and_variable_lengths' - Wildcard with Variable",
     assert_equals_int (count_wild_and_variable_lengths (TupleP [Wildcard, Variable "name"]), 5));

test("'count_wild_and_variable_lengths' - only ConstP",
     assert_equals_int (count_wild_and_variable_lengths (ConstP 42), 0));

test("'count_wild_and_variable_lengths' - Nested ConstructorP with Wildcard and Variable",
     assert_equals_int (count_wild_and_variable_lengths (ConstructorP ("cons", TupleP [ConstP 42, ConstructorP ("cons2", Wildcard), Variable "var", Wildcard, UnitP])), 5));

(* count_some_var *)
test("'count_some_var' - single Variable",
     assert_equals_int (count_some_var ("name", Variable "name"), 1));

test("'count_some_var' - list of Variable",
     assert_equals_int (count_some_var ("name", TupleP [Variable "name", Variable "name2", Variable "name", UnitP]), 2));

test("'count_some_var' - Nested ConstructorP ",
     assert_equals_int (count_some_var ("var", ConstructorP ("cons", TupleP [ConstP 42, ConstructorP ("cons2", Wildcard), Variable "var", Wildcard, UnitP])), 1));

test("'count_some_var' - ConstP",
     assert_equals_int (count_some_var ("", ConstP 42), 0));

(* match *)
test("'match' - Wildcard pattern",
     assert_equals_string_valu_list_option (match (Const 42, Wildcard), SOME []));

test("'match' - Variable pattern",
     assert_equals_string_valu_list_option (match (Const 42, Variable "d"), SOME [("d", Const 42)]));

test("'match' - mismatch Constructor",
     assert_equals_string_valu_list_option (
       match (
         Constructor ("cons", Const 42),
         ConstructorP ("cons", TupleP [ConstP 42, ConstructorP ("cons2", Wildcard), Variable "var", Wildcard, UnitP])),
         NONE));

test("'match' - Constructor",
     assert_equals_string_valu_list_option (
       match (
         Constructor ("cons", Tuple [Const 42, Constructor ("cons2", Unit), Const 43, Unit, Unit]),
         ConstructorP ("cons", TupleP [ConstP 42, ConstructorP ("cons2", Wildcard), Variable "var", Wildcard, UnitP])),
         SOME [("var",Const 43)]));

run();