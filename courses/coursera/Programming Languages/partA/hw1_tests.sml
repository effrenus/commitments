use "./test-lib/testing.sml";
use "./hw1.sml";

open SmlTests;

(* is_older *)
test("'is_older' - compare dates",
     assert_true (is_older ((2022, 1, 12), (2022, 1, 15))));

test("'is_older' - compare dates)",
     assert_false (is_older ((2022, 1, 12), (2021, 1, 15))));

(* number_in_month *)
test("'number_in_month' - match exists",
     assert_equals_int(number_in_month([(2012,2,28), (2013,2,1), (2010,3,1)], 2), 2));

test("'number_in_month' - empty list",
     assert_equals_int(number_in_month([], 12), 0));

test("'number_in_month' - zero match",
     assert_equals_int(number_in_month([(2012,4,12)], 12), 0));

(* number_in_months *)
test("'number_in_months' - match exists",
     assert_equals_int(number_in_months([(2012,2,28), (2013,2,1), (2010,3,1)], [2, 3]), 3));

test("'number_in_months' - empty list",
     assert_equals_int(number_in_months([], [2]), 0));

test("'number_in_months' - empty list of months",
     assert_equals_int(number_in_months([], []), 0));

test("'number_in_months' - zero exists",
     assert_equals_int(number_in_months([(2012,2,28), (2013,2,1), (2010,3,1)], [10, 12]), 0));

(* dates_in_month *)
test("'dates_in_month' - match exists",
     assert_equals_tuple3_int_list(dates_in_month([(2012,2,28), (2013,2,1), (2010,3,2), (2010,3,1)], 3), [(2010,3,2), (2010,3,1)]));

test("'dates_in_month' - empty dates list",
     assert_equals_tuple3_int_list(dates_in_month([], 3), []));

test("'dates_in_month' - zero match",
     assert_equals_tuple3_int_list(dates_in_month([(2012,2,28), (2013,2,1), (2010,3,1)], 5), []));

(* dates_in_months *)
test("'dates_in_months' - match exists",
     assert_equals_tuple3_int_list(
          dates_in_months([(2012,2,28), (2013,2,1), (2010,3,1)], [3, 2]),
          [(2010,3,1), (2012,2,28), (2013,2,1)]
     )
);

test("'dates_in_months' - empty dates list",
     assert_equals_tuple3_int_list(dates_in_months([], [3, 2]), []));

test("'dates_in_months' - zero match",
     assert_equals_tuple3_int_list(dates_in_months([(2013,11,1)], [3, 10]), []));

(* date_to_string *)
test("'date_to_string' - correctly convert date to string",
     assert_equals_string(date_to_string (2013, 1, 20), "January 20, 2013"));

test("'date_to_string' - correctly convert date to string",
     assert_equals_string(date_to_string (2022, 12, 31), "December 31, 2022"));

(* number_before_reaching_sum *)
test("'number_before_reaching_sum' - positive result case",
     assert_equals_int(number_before_reaching_sum (10, [5, 1, 5]), 2));

test("'number_before_reaching_sum' – sum reacher after first element",
     assert_equals_int(number_before_reaching_sum (10, [15, 1, 5]), 0));

test("'number_before_reaching_sum' – initial sum is 0",
     assert_equals_int(number_before_reaching_sum (0, []), ~1));

(* what_month *)
test("'what_month' - Jan",
     assert_equals_int(what_month 10, 1));

test("'what_month' - Feb",
     assert_equals_int(what_month 40, 2));

test("'what_month' - Mar",
     assert_equals_int(what_month 60, 3));

test("'what_month' - Dec",
     assert_equals_int(what_month 360, 12));

(* month_range *)
test("'month_range' - Jan-Feb",
     assert_equals_int_list(month_range (30, 33), [1, 1, 2, 2]));

test("'month_range' - Dec one day",
     assert_equals_int_list(month_range (360, 360), [12]));

(* oldest *)
test("'oldest' - empty list",
     assert_equals_tuple3_int_list_option(oldest [], NONE));

test("'oldest' - list with one element",
     assert_equals_tuple3_int_list_option(oldest [(2020, 10, 8)], SOME (2020, 10, 8)));

test("'oldest' - list with several elements",
     assert_equals_tuple3_int_list_option(oldest [(2020, 10, 8), (2007, 3, 8), (2008, 10, 28)], SOME (2007, 3, 8)));

(* number_in_months_challenge *)
test("'number_in_months_challenge' - match exists",
     assert_equals_int(number_in_months_challenge([(2012,2,28), (2013,2,1), (2010,3,1)], [2, 3, 2, 3]), 3));

test("'number_in_months_challenge' - match exists",
     assert_equals_int(number_in_months_challenge([(2012,2,28), (2013,2,1), (2010,3,1)], [2, 2, 2, 2]), 2));

test("'number_in_months_challenge' - empty list",
     assert_equals_int(number_in_months_challenge([], [2, 2]), 0));

test("'number_in_months_challenge' - empty list of months",
     assert_equals_int(number_in_months_challenge([], []), 0));

test("'number_in_months_challenge' - zero exists",
     assert_equals_int(number_in_months_challenge([(2012,2,28), (2013,2,1), (2010,3,1)], [10, 12, 12, 12]), 0));

(* dates_in_months_challenge *)
test("'dates_in_months_challenge' - match exists",
     assert_equals_tuple3_int_list(
          dates_in_months_challenge([(2012,2,28), (2013,2,1), (2010,3,1)], [3, 3, 3, 2]),
          [(2010,3,1), (2012,2,28), (2013,2,1)]
     )
);

test("'dates_in_months_challenge' - empty dates list",
     assert_equals_tuple3_int_list(dates_in_months_challenge([], [3, 2, 2]), []));

test("'dates_in_months_challenge' - zero match",
     assert_equals_tuple3_int_list(dates_in_months_challenge([(2013,11,1)], [3, 10, 10]), []));

run();