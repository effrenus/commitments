exception Implement

(*
  Date format (tuple): (year, month, day) *)

(* 
  Takes two dates and evaluates to true or false. It evaluates to true if
  the first argument is a date that comes before the second argument. *)
fun is_older (d1 : int * int * int, d2 : int * int * int) =
  let
    val days1 = (#1 d1 - 1) * 12 * 365 + (#2 d1 - 1) * 31 + #3 d1
    val days2 = (#1 d2 - 1) * 12 * 365 + (#2 d2 - 1) * 31 + #3 d2
  in
    if days1 < days2 then true else false
  end

(* 
  Takes a list of dates and a month (i.e., an int) and returns
  how many dates in the list are in the given month. *)
fun number_in_month (ds : (int * int * int) list, month : int) =
  if null ds
  then
    0
  else
    (if #2 (hd ds) = month then 1 else 0) + number_in_month (tl ds, month)

(* 
  Takes a list of dates and a list of months (i.e., an int list)
  and returns the number of dates in the list of dates that are in any of the months in the list of months.
  Assume the list of months has no number repeated. *)
fun number_in_months (ds : (int * int * int) list, ms : int list) =
  if null ms
  then
    0
  else
    (number_in_month (ds, hd ms)) + (number_in_months (ds, tl ms))

(* 
  Takes a list of dates and a month (i.e., an int) and returns a
  list holding the dates from the argument list of dates that are in the month. The returned list should
  contain dates in the order they were originally given. *)
fun dates_in_month (ds : (int * int * int) list, m : int) =
  if null ds
  then
    []
  else
    if #2 (hd ds) = m then (hd ds) :: dates_in_month (tl ds, m) else dates_in_month (tl ds, m)

(* 
  Takes a list of dates and a list of months (i.e., an int list)
  and returns a list holding the dates from the argument list of dates that are in any of the months in
  the list of months. Assume the list of months has no number repeated. *)
fun dates_in_months (ds : (int * int * int) list, ms : int list) =
  if null ms
  then []
  else dates_in_month (ds, hd ms) @ dates_in_months (ds, tl ms)

(* 
  Takes a list of strings and an int n and returns the nth element of the
  list where the head of the list is 1st. *)
fun get_nth (ls : 'a list, n : int) =
  if n = 1
  then hd ls
  else get_nth (tl ls, n - 1)

(*
  Takes a date and returns a string of the form 'January 20, 2013' *)
fun date_to_string (d : int * int * int) =
  let
    val month_names = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
  in
    get_nth (month_names, #2 d) ^ " " ^ Int.toString (#3 d) ^ ", " ^ Int.toString (#1 d)
  end

(* 
  Takes an int called sum, which you can assume is positive,
  and an int list, which you can assume contains all positive numbers, and returns an int.
  Should return an int n such that the first n elements of the list add to less than sum, but the first
  n + 1 elements of the list add to sum or more. *)
fun number_before_reaching_sum (sum : int, ls : int list) =
  if sum <= 0
  then ~1
  else 1 + number_before_reaching_sum(sum - (hd ls), tl ls)

(* 
  Takes a day of year (i.e., an int between 1 and 365) and returns
  what month that day is in (1 for January, 2 for February, etc.). *)
fun what_month (d : int) =
  let
    val days_in_month = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
  in
    number_before_reaching_sum (d, days_in_month)
  end

(* 
  Ttakes two days of the year day1 and day2 and returns an int list
  [m1,m2,...,mn] where m1 is the month of day1, m2 is the month of day1+1, ..., and mn is the month of day day2 *)
fun month_range (day1 : int, day2 : int) =
  if day1 > day2
  then []
  else what_month day1 :: month_range (day1 + 1, day2)

(* 
  Takes a list of dates and evaluates to an (int*int*int) option.
  It evaluates to NONE if the list has no dates and SOME d if the date d is the oldest date in the list. *)
fun oldest (ds : (int * int * int) list) =
  if null ds
  then NONE
  else 
    let
      val next_oldest = oldest (tl ds)
    in
      SOME (
          if isSome next_oldest then
            if is_older (hd ds, valOf next_oldest) then hd ds else valOf next_oldest
          else
            hd ds
        )
    end

(* 
  Returns list with unique values.
  Not very performant solution. *)
fun unique vs : int list = case vs of [] => [] | v::vs' => if List.exists (fn v' => v = v') vs' then unique vs' else v::unique vs'

(* 
  Takes a list of dates and a list of months (i.e., an int list)
  and returns the number of dates in the list of dates that are in any of the months in the list of months.
  Assume the list of months may has repeated numbers. *)
fun number_in_months_challenge (ds : (int * int * int) list, ms : int list) = number_in_months (ds, unique ms)

(* 
  Takes a list of dates and a list of months (i.e., an int list)
  and returns a list holding the dates from the argument list of dates that are in any of the months in
  the list of months. List of months may has repeated numbers. *)
fun dates_in_months_challenge (ds : (int * int * int) list, ms : int list) = dates_in_months (ds, unique ms)

(* 
  Takes a date and determines if it describes a real date in the common era *)
fun reasonable_date () =
  (* TODO: Implement *) raise Implement