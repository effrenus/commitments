use "./test-lib/testing.sml";
use "./hw2.sml";

open SmlTests;

(* all_except_option *)
test("'all_except_option'",
  assert_equals_string_list_option (all_except_option ("hello", ["hello", "world"]), SOME ["world"]));

test("'all_except_option'",
  assert_equals_string_list_option (all_except_option ("hello", ["world"]), NONE));

(* get_substitutions1 *)
test("'get_substitutions1'",
  assert_equals_string_list (get_substitutions1 ([["hello", "world"], ["hello", "!", "hello"]], "hello"), ["world", "!", "hello"]));

test("'get_substitutions1' - empty list",
  assert_equals_string_list (get_substitutions1 ([], "hello"), []));

test("'get_substitutions1' - no match",
  assert_equals_string_list (get_substitutions1 ([["!!!"], ["???"]], "hello"), []));

test("'get_substitutions1' - list with only one matched string",
  assert_equals_string_list (get_substitutions1 ([["hello"], ["hello"]], "hello"), []));

(* get_substitutions2 *)
test("'get_substitutions2'",
  assert_equals_string_list (get_substitutions2 ([["hello", "world"], ["hello", "!", "hello"]], "hello"), ["world", "!", "hello"]));

test("'get_substitutions2' - empty list",
  assert_equals_string_list (get_substitutions2 ([], "hello"), []));

test("'get_substitutions2' - no match",
  assert_equals_string_list (get_substitutions2 ([["!!!"], ["???"]], "hello"), []));

test("'get_substitutions2' - list with only one matched string",
  assert_equals_string_list (get_substitutions2 ([["hello"], ["hello"]], "hello"), []));

(* similar_names *)
test("'similar_names'",
  assert_equals_any (similar_names ([["!!!", "first"]], {first="first", middle="middle", last="last"}), [{first="first", middle="middle", last="last"}, {first="!!!", middle="middle", last="last"}]));

test("'similar_names' - no match",
  assert_equals_any (similar_names ([["!!!"]], {first="first", middle="middle", last="last"}), [{first="first", middle="middle", last="last"}]));

test("'similar_names' - empty substitution list",
  assert_equals_any (similar_names ([], {first="first", middle="middle", last="last"}), [{first="first", middle="middle", last="last"}]));

test("'similar_names' - several matches",
  assert_equals_any (
    similar_names ([["!!!", "first"], ["???", "first"]], {first="first", middle="middle", last="last"}),
    [{first="first", middle="middle", last="last"}, {first="!!!", middle="middle", last="last"}, {first="???", middle="middle", last="last"}]
  ));

(* card_color *)
test("'card_color'",
  assert_equals_any (card_color (Spades, Jack), Black));

test("'card_color'",
  assert_equals_any (card_color (Clubs, Num 8), Black));

test("'card_color'",
  assert_equals_any (card_color (Diamonds, Queen), Red));

test("'card_color'",
  assert_equals_any (card_color (Hearts, Ace), Red));

(* card_value *)
test("'card_value'",
  assert_equals_int (card_value (Spades, Ace), 11));

test("'card_value'",
  assert_equals_int (card_value (Diamonds, Num 8), 8));

List.map (fn v => test("'card_value'",
  assert_equals_int (card_value (Diamonds, v), 10))) [Queen, King, Jack];

(* remove_card *)
exception RemoveCardTestExn;

test("'card_color'",
  assert_equals_any (remove_card ([(Spades, Jack)], (Spades, Jack), RemoveCardTestExn), []));

test("'card_color'",
  assert_equals_any (remove_card ([(Spades, Jack), (Spades, Queen)], (Spades, Jack), RemoveCardTestExn), [(Spades, Queen)]));

test("'card_color' - illegal move",
  assert_raises (remove_card, ([(Spades, Queen)], (Spades, Jack), RemoveCardTestExn), RemoveCardTestExn));

test("'card_color' - illegal move",
  assert_raises (remove_card, ([], (Spades, Jack), RemoveCardTestExn), RemoveCardTestExn));

(* all_same_color *)
test("'all_same_color' - same colors",
  assert_equals_bool (all_same_color [(Spades, Jack), (Clubs, Queen)], true));

test("'all_same_color' - diff colors",
  assert_equals_bool (all_same_color [(Spades, Jack), (Hearts, Queen)], false));

test("'all_same_color' - empty list",
  assert_equals_bool (all_same_color [], true));

(* sum_cards *)
test("'sum_cards' - empty",
  assert_equals_int (sum_cards [], 0));

test("'sum_cards'",
  assert_equals_int (sum_cards [(Hearts, Num 6), (Clubs, King)], 16));

test("'sum_cards'",
  assert_equals_int (sum_cards [(Hearts, Num 6), (Clubs, King), (Clubs, Queen), (Clubs, Ace)], 37));

(* score *)
test("'score'",
  assert_equals_int (score ([(Hearts, Num 6), (Clubs, King), (Clubs, Queen), (Clubs, Ace)], 20), 51));

test("'score' - empty card list",
  assert_equals_int (score ([], 20), 10));

test("'score' - empty card list",
  assert_equals_int (score ([(Hearts, Num 2), (Clubs, Num 4)], 10), 4));

(* officiate *)
test("'officiate'",
  assert_equals_int (officiate ([(Clubs,Ace),(Spades,Ace),(Clubs,Ace),(Spades,Ace)], [Draw,Draw,Draw,Draw,Draw], 42), 3));

test("'officiate' - illegal move",
  assert_raises (officiate, ([(Clubs,Jack),(Spades,Num(8))], [Draw, Discard(Hearts,Jack)], 42), IllegalMove));

run();