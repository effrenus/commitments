(*
  Compares two strings. *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

(* 
  Takes a string and a string list. Return NONE if the
  string is not in the list, else return SOME lst where lst is identical to the argument list except the string
  is not in it. You may assume the string is in the list at most once. *)
fun all_except_option (s : string, ls : string list) =
  case ls of
    [] => NONE
    | ss::ls' => 
        if same_string(s, ss)
        then SOME ls'
        else case all_except_option(s, ls') of
                  NONE => NONE
                | SOME(ls) => SOME(ss::ls)

(* 
  Takes a string list list (a list of list of strings, the
  substitutions) and a string s and returns a string list. The result has all the strings that are in
  some list in substitutions that also has s, but s itself should not be in the result. *)
fun get_substitutions1 (subs : string list list, s : string) =
  case subs of
      [] => []
    | ls::subs' =>
      let
        val ls' = case all_except_option (s, ls) of NONE => [] | SOME(v) => v
      in
        ls' @ get_substitutions1(subs', s)
      end

(* 
  Like get_substitutions1 except it uses a tail-recursive
  local helper function. *)
fun get_substitutions2 (subs : string list list, s : string) =
  let
    fun aux (acc : string list, subs : string list list) =
      case subs of
          [] => acc
        | l::subs' => case all_except_option(s, l) of
            NONE => aux(acc, subs')
          | SOME(v) => aux(acc @ v, subs')
    in
      aux([], subs)
    end

type fullname = {first : string, middle : string, last : string}

(* 
  Takes a string list list of substitutions (as in parts (b) and
  (c)) and a full name of type {first:string, middle:string, last:string} and returns a list of full
  names (type {first:string, middle:string, last:string} list). The result is all the full names you
  can produce by substituting for the first name (and only the first name) using substitutions and parts (b)
  or (c). *)
fun similar_names (subs : string list list, fullname : fullname) : fullname list = 
  let
    val middle = #middle fullname
    val last = #last fullname
    fun aux (name_subs: string list) =
      case name_subs of
          [] => []
        | name::name_subs' => {first=name, middle=middle, last=last}::aux name_subs'
  in
    fullname::aux (get_substitutions2(subs, #first fullname))
  end

(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw 

exception IllegalMove

(* 
  Takes a card and returns its color (spades and clubs are black, diamonds and hearts are red). *)
fun card_color (card : card) : color =
  case card of
      (Spades, _) => Black
    | (Clubs, _) => Black
    | (Diamonds, _) => Red
    | (Hearts, _) => Red

(* 
  Takes a card and returns its value (numbered cards have their number as the value, aces are 11, everything else is 10). *)
fun card_value (card : card) =
  case card of
      (_, Ace) => 11
    | (_, Num(v)) => v
    | _ => 10

(* 
  Takes a list of cards cs, a card c, and an exception e. It returns a
  list that has all the elements of cs except c. If c is in the list more than once, remove only the first one.
  If c is not in the list, raise the exception e. *)
fun remove_card (cs : card list, c : card, e : exn) =
  case cs of
      [] => raise e
    | c'::cs' => if c' = c then cs' else c'::remove_card (cs', c, e)

(* 
  Takes a list of cards and returns true if all the cards in the list are the same color. *)
fun all_same_color (cs : card list) =
  case cs of
    [] => true
    | c1::[] => true
    | c1::cs' => 
        let
          val c2::_ = cs'
        in
          if card_color c1 <> card_color c2 then false else all_same_color cs'
        end

(* 
  Takes a list of cards and returns the sum of their values. *)
fun sum_cards (cs : card list) =
  let
    fun aux (acc : int, cs : card list) =
      case cs of
        [] => acc
      | c::cs' => aux(acc + card_value c, cs')
  in
    aux(0, cs)
  end

(* 
  Takes a card list (the held-cards) and an int (the goal) and computes the score. *)
fun score (cs : card list, goal : int) =
  let
    val sum = sum_cards (cs)
    val pre_score = if sum > goal then 3 * (sum - goal) else goal - sum
  in
    if all_same_color(cs) then pre_score div 2 else pre_score
  end

(* 
  It takes a card list (the card-list) a move list
  (what the player “does” at each point), and an int (the goal) and returns the score at the end of the
  game after processing (some or all of) the moves in the move list in order.
  
  * The game starts with the held-cards being the empty list.
  * The game ends if there are no more moves. (The player chose to stop since the move list is empty.)
  * If the player discards some card c, play continues (i.e., make a recursive call) with the held-cards
    not having c and the card-list unchanged. If c is not in the held-cards, raise the IllegalMove
    exception.
  * If the player draws and the card-list is (already) empty, the game is over. Else if drawing causes
    the sum of the held-cards to exceed the goal, the game is over (after drawing). Else play continues
    with a larger held-cards and a smaller card-list. *)
fun officiate (cs : card list, ms : move list, goal : int) =
  let
    fun aux (hold : card list, ms : move list, cs : card list) = 
      case ms of
          [] => score(hold, goal)
        | m::ms' => case m of
                        Discard(c) => aux(remove_card(hold, c, IllegalMove), ms', cs)
                      | Draw => case cs of
                                    [] => score(hold, goal)
                                  | c::cs' => if score(c::hold, goal) >= goal then score(c::hold, goal) else aux(c::hold, ms', cs')
  in
    aux([], ms, cs)
  end