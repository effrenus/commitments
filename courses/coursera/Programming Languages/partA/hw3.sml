use "./hw3_types.sml";

fun g f1 f2 p =
    let 
	val r = g f1 f2 
    in
	case p of
	    Wildcard          => f1 ()
	  | Variable x        => f2 x
	  | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
	  | ConstructorP(_,p) => r p
	  | _                 => 0
    end

(*
	Takes a string list and returns a string list that has only
	the strings in the argument that start with an uppercase letter. *)
val only_capitals =
	List.filter (fn s => Char.isUpper (String.sub (s, 0)))

(*
	Takes a string list and returns the longest string in the
	list. If the list is empty, return "". *)
val longest_string1 =
	List.foldl (fn (s, longest) => if String.size s > String.size longest then s else longest) ""

(*
	Takes a string list and returns the longest string in the
	list. If the list is empty, return "".
	In the case of ties it returns the string closest to the end of the list*)
val longest_string2 =
	List.foldl (fn (s, longest) => if String.size s >= String.size longest then s else longest) ""

val longest_capitalized =
	longest_string1 o only_capitals

(* DRY: using helper to avoid duplicate code. *)
fun longest_string_helper f ls =
	List.foldl (fn (s, longest) => if f (s, longest) then s else longest) "" ls

val longest_string3 =
	longest_string_helper (fn (s, s') => String.size s > String.size s')

val longest_string4 =
	longest_string_helper (fn (s, s') => String.size s >= String.size s')

(*
	Takes a string and returns the string that is the same characters in
	reverse order. *)
val rev_string =
	String.implode o List.rev o String.explode

(* 
	The first argument should be applied to elements of the second argument in order
	until the first time it returns SOME v for some v and then v is the result of the call to first_answer.
	If the first argument returns NONE for all list elements, then first_answer should raise the exception
	NoAnswer. *)
fun first_answer f ls =
	case ls of
			[] => raise NoAnswer
		| v::ls' => case f v of NONE => first_answer f ls' | SOME v' => v'

(*
	The first argument should be applied to elements of the second argument.
	If it returns NONE for any element, then the result for all_answers is NONE. Else the
	calls to the first argument will have produced SOME lst1, SOME lst2, lstn and the result of
	all_answers is SOME lst where lst is lst1, lst2 appended together (order doesn’t matter).*)
fun all_answers f lst =
	let
		fun apply lst acc =
			case lst of
					[] => SOME acc
				|	v::lst' => case f v of
												NONE => NONE
											|	SOME(r) => apply lst' (r @ acc)
	in
		apply lst []
	end

(* 
	Takes a pattern and returns how many Wildcard patterns it contains. *)
val count_wildcards =
	g (fn () => 1) (fn _ => 0)

(* 
	Takes a pattern and returns the number of Wildcard patterns it contains plus the sum 
	of the string lengths of all the variables in the variable patterns it contains. *)
val count_wild_and_variable_lengths =
	g (fn () => 1) String.size

(*
	Takes a string and a pattern (as a pair) and
	returns the number of times the string appears as a variable in the pattern. We care only about
	variable names; the constructor names are not relevant. *)
fun count_some_var (s, p) =
	g (fn () => 0) (fn n => if n = s then 1 else 0) p

(* 
	Takes a pattern and returns true if and only if all the variables
	appearing in the pattern are distinct from each other (i.e., use different strings). The constructor
	names are not relevant. *)
fun check_pat p =
	let
		fun get_names p =
			case p of
					Variable x => [x]
				|	TupleP ps => List.foldl (fn (p,ls) => (get_names p) @ ls) [] ps
				| ConstructorP(_,p) => get_names p
				| _ => []
		fun has_unique ls =
			case ls of
					[] => true
				| v::ls' => if List.exists (fn v' => v = v') ls' then false else has_unique ls'
	in
		(has_unique o get_names) p
	end

(* 
	Takes a valu * pattern and returns a (string * valu) list option,
	namely NONE if the pattern does not match and SOME lst where lst is the list of bindings if it does.
	Note that if the value matches but the pattern has no patterns of the form Variable s, then the result
	is SOME []. *)
fun match (vv, p) =
	case (p, vv) of
			(Wildcard, _) => SOME []
		|	(Variable s, _) => SOME [(s, vv)]
		| (UnitP, Unit) => SOME []
		| (ConstP v, Const v') => if v = v' then SOME [] else NONE
		| (ConstructorP (s, p), Constructor (s', v)) => if s = s' then match (v, p) else NONE
		| (TupleP ps, Tuple vs) => if List.length ps = List.length vs then all_answers match (ListPair.zip (vs, ps)) else NONE
		| (_, _) => NONE

fun first_match v ps =
	SOME (first_answer (fn p => match (v, p)) ps) handle NoAnswer => NONE