# Programming Languages

Author: [Dan Grossman](https://homes.cs.washington.edu/~djg/)

## Parts

- [Programming Languages, Part A](https://www.coursera.org/learn/programming-languages)
- [Programming Languages, Part B](https://www.coursera.org/learn/programming-languages-part-b)
- [Programming Languages, Part C](https://www.coursera.org/learn/programming-languages-part-c)

## Languages

- [Standard ML](https://en.wikipedia.org/wiki/Standard_ML)
- [Racket](<https://en.wikipedia.org/wiki/Racket_(programming_language)>)
- [Ruby](<https://en.wikipedia.org/wiki/Ruby_(programming_language)>)
