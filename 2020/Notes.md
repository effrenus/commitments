## Fingerprint

### Review

TBD

### References

* [Fingerprinting the Fingerprinters](https://www.computer.org/csdl/pds/api/csdl/proceedings/download-article/1mbmHGY5Lpu/pdf)
* [Pixel Perfect: Fingerprinting Canvas in HTML5](https://hovav.net/ucsd/dist/canvas.pdf)
* [Detecting Privacy Badger’s Canvas FP detection](https://adtechmadness.wordpress.com/2020/03/27/detecting-privacy-badgers-canvas-fp-detection/)
* [Behind theOne-Way Mirror](https://www.eff.org/files/2019/12/11/behind_the_one-way_mirror-a_deep_dive_into_the_technology_of_corporate_surveillance.pdf)

### Observations

* HH uses `fingerprint2` library.
* WASD uses handmade primitive fingerprint generation methods.

## Info snippets

* New image format AVIF (based on AV1 video codec) better than WebP compression, supporting alpha, animation. Support landed soon in Chrome and Firefox.
* `brunsli` repack losslessly to JPEG XL and can be reverted back to original JPEG if client doesn't support JPEG XL. WASM could be used for unpack in browser. 22% size reduction. [brunsli](https://github.com/google/brunsli)
* `<link rel="reconnect" crossorigin>` and `<link rel="reconnect">` different request. So if resource fetching w/o CORS will be used second connection. [https://crenshaw.dev/preconnect-resource-hint-crossorigin-attribute/](https://crenshaw.dev/preconnect-resource-hint-crossorigin-attribute/)

## SocketIO protocol

* https://github.com/socketio/engine.io-protocol
* https://github.com/socketio/socket.io-protocol

## UCS-2 (Unicode char set)

UCS-2 is obsolete encoding used prior Unicode 2.0. It's uses fixed length (2 bytes) code points, doesn't support surrogate pairs/normalization.
Recommended to use UTF-16 variable length encoding (2-4 bytes).

Details: https://unicode.org/faq/utf_bom.html#utf16-11

## To check

* OpenMP, MPI
* Обобщенное программирование
* [What Every Computer Scientist Should Know About Floating-Point Arithmetic](https://www.itu.dk/~sestoft/bachelor/IEEE754_article.pdf) by David Goldberg
* ✅ UCS-2 (Unicode)
* Entropy measure (bits)