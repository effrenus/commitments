## Books

* ✅ [Effective TypeScript](https://www.oreilly.com/library/view/effective-typescript/9781492053736/) ⭐️⭐️⭐️⭐️
* ✅ [Deep JavaScript: Theory and techniques](https://exploringjs.com/deep-js/) ⭐️⭐️⭐️⭐️
* ✅ [Tackling TypeScript](https://exploringjs.com/tackling-ts/index.html) by Axel Rauschmayer ⭐️⭐️⭐️⭐️
* ⏳ [Guide to Competitive Programming](https://www.springer.com/us/book/9783319725475)
* ☑️ [Чистая архитектура. Искусство разработки программного обеспечения](https://www.ozon.ru/context/detail/id/144499396/)
* 🔁 [Designing Data-Intensive Applications](https://www.oreilly.com/library/view/designing-data-intensive-applications/9781491903063/)
* ☑️ [Domain Driven Design Quickly](http://carfield.com.hk/document/software%2Bdesign/dddquickly.pdf)
* ✅ [JavaScript: The Definitive Guide (7th Edition)](https://www.oreilly.com/library/view/javascript-the-definitive/9781491952016/) by David Flanagan ⭐️⭐️⭐️⭐️
## White papers
* ☑️ [A Survey of Monte Carlo Tree Search Methods](http://www.incompleteideas.net/609%20dropbox/other%20readings%20and%20resources/MCTS-survey.pdf)
## Pet projects
* NES emulator
* Raft protocol implementation
